#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 10 09:09:21 2022

@author: jmcelroy
"""

n1 = 0
n2 = 1
n3 = 0

fib_seq = [n1, n2]

while n3 < 4000000:
    n3 = n2 + n1
    n1 = n2
    n2 = n3
    fib_seq.append(n3)
    
print(fib_seq)

ans = 0
for i in fib_seq:
    if i % 2 == 0:
        ans += i
        
print(ans)