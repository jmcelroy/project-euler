#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 10 09:32:47 2022

@author: jmcelroy
"""

number = 600851475143
factors = []

from math import sqrt

for i in range(2, int(sqrt(number))):
    if number % i == 0:
        #print(i)
        factors.append(i)
        
#print(factors)

prime_factors = []
for i in range(0, len(factors)):
    for j in range(0, len(factors)):
        if factors[i] % factors[j] == 0:
            prime_factors.append(factors[j])
            break
        
#print(prime_factors)
        
prime_factors_no_duplicates = []
for i in prime_factors:
    if i not in prime_factors_no_duplicates:
        prime_factors_no_duplicates.append(i)

#print(prime_factors_no_duplicates)
print(prime_factors_no_duplicates[-1])