#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 10 08:52:47 2022

@author: jmcelroy
"""

import math

sum1 = 0
sum2 = 0
sum3 = 0

for i in range(0, 1000):
    if (i % 3 == 0) and (i % 5 != 0):
        sum1 += i
    if (i % 3 != 0) and (i % 5 == 0):
        sum2 += i
    if (i % 3 == 0) and (i % 5 == 0):
        sum3 += i
        
print(sum1+sum2+sum3)